package com.example.sam.springcloud.sqs.domain;

public enum IpnType {
    PAY, PAYOUT
}
