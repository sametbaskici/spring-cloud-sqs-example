package com.example.sam.springcloud.sqs.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.config.SimpleMessageListenerContainerFactory;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Configuration
public class MyAwsConfig {

    private static final String deadLetterQueueName = "ipnDeadLetterQueue.fifo";

    @Primary
    @Qualifier("amazonSQSAsync")
    @Bean
    public AmazonSQSAsync amazonSQSAsync(
            @Value("${cloud.aws.credentials.accessKey}") String accessKey,
            @Value("${cloud.aws.credentials.secretKey}") String secretKey,
            @Value("${cloud.aws.region.static}") String region) {

        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return AmazonSQSAsyncClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
    }

    @Bean
    @Primary
    public SimpleMessageListenerContainerFactory simpleMessageListenerContainerFactory(@Qualifier("amazonSQSAsync") AmazonSQSAsync amazonSqsAsync) {
        SimpleMessageListenerContainerFactory factory = new SimpleMessageListenerContainerFactory();
        factory.setAmazonSqs(amazonSqsAsync);
        factory.setMaxNumberOfMessages(5);
        return factory;
    }

    @Primary
    @Bean
    public QueueMessagingTemplate queueMessagingTemplate(@Qualifier("amazonSQSAsync") AmazonSQSAsync amazonSqsAsync) {

        //createDeadLetterQueue(amazonSqsAsync);
        return new QueueMessagingTemplate(amazonSqsAsync);
    }

    public void createDeadLetterQueue(AmazonSQSAsync amazonSQSAsync) {

        final String sourceQueueUrl = amazonSQSAsync.getQueueUrl("gamze-queue.fifo").getQueueUrl();
        final String deadLetterQueueUrl = amazonSQSAsync.getQueueUrl(deadLetterQueueName).getQueueUrl();

        final GetQueueAttributesResult deadLetterQueueAttributes = amazonSQSAsync.getQueueAttributes(
                new GetQueueAttributesRequest(deadLetterQueueUrl)
                        .withAttributeNames("QueueArn"));

        final String deadLetterQueueArn = deadLetterQueueAttributes.getAttributes().get("QueueArn");

        final SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(sourceQueueUrl)
                .addAttributesEntry(QueueAttributeName.RedrivePolicy.toString(),
                        "{\"maxReceiveCount\":\"5\", \"deadLetterTargetArn\":\""
                                + deadLetterQueueArn + "\"}");
        amazonSQSAsync.setQueueAttributes(request);

    }

}
