package com.example.sam.springcloud.sqs.controller;

import com.example.sam.springcloud.sqs.dto.IpnDTO;
import com.example.sam.springcloud.sqs.repository.DeadLetterIpnRepository;
import com.example.sam.springcloud.sqs.service.QueueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/merchant")
public class MerchantPaymentController {

    private  final DeadLetterIpnRepository deadLetterIpnRepository;
    private final QueueService queueService;

    @Autowired
    public MerchantPaymentController(DeadLetterIpnRepository deadLetterIpnRepository, QueueService queueService) {
        this.deadLetterIpnRepository = deadLetterIpnRepository;
        this.queueService = queueService;
    }


    public void handleTransaction(){

    }


    @SqsListener(value = "IpnQueue.fifo", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    private void merchantIpnListener(IpnDTO ipn, @Headers Map<String,Object> headers){

        //TODO Send ipn to merchant

        if(true) throw new RuntimeException("listener exception"); //TODO REMOVE

        log.info(ipn.toString());
        //TODO Send ipn to merchant

    }

}
