package com.example.sam.springcloud.sqs.service;

import com.example.sam.springcloud.sqs.domain.IpnType;

import java.util.List;

public interface QueueService<T extends Object> {

    void send(T object);

    List<T>  getDeadLetterIpns(IpnType type);

}
