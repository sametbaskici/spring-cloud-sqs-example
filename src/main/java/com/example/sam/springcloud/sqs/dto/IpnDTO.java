package com.example.sam.springcloud.sqs.dto;

import com.example.sam.springcloud.sqs.domain.IpnType;
import com.example.sam.springcloud.sqs.domain.TransactionStatus;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
public class IpnDTO {

    private Long id;
    private IpnType ipnType;
    private Long paymentId;
    private String orderId;
    private TransactionStatus transactionStatus;

}
