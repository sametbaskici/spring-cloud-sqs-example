package com.example.sam.springcloud.sqs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudSqsExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudSqsExampleApplication.class, args);
    }
}
