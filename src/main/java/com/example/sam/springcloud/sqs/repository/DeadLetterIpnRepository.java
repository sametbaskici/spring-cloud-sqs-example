package com.example.sam.springcloud.sqs.repository;

import com.example.sam.springcloud.sqs.domain.DeadLetterIpn;
import com.example.sam.springcloud.sqs.domain.IpnType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeadLetterIpnRepository extends JpaRepository<DeadLetterIpn, Long> {
    List<DeadLetterIpn> findByIpnType(IpnType ipnType);
    Optional<DeadLetterIpn> findById(Long id);

    default DeadLetterIpn find(Long id){
        return findById(id)
                .orElseThrow(RuntimeException::new);
    }
}
