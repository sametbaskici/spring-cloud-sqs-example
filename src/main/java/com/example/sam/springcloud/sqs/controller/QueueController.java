package com.example.sam.springcloud.sqs.controller;

import com.example.sam.springcloud.sqs.domain.DeadLetterIpn;
import com.example.sam.springcloud.sqs.domain.IpnType;
import com.example.sam.springcloud.sqs.domain.TransactionStatus;
import com.example.sam.springcloud.sqs.dto.IpnDTO;
import com.example.sam.springcloud.sqs.repository.DeadLetterIpnRepository;
import com.example.sam.springcloud.sqs.service.IpnQueueService;
import com.example.sam.springcloud.sqs.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/queue")
@RestController
public class QueueController {

   private final IpnQueueService ipnQueueService;
   private final DeadLetterIpnRepository deadLetterIpnRepository;

    public QueueController(IpnQueueService ipnQueueService, DeadLetterIpnRepository deadLetterIpnRepository) {
        this.ipnQueueService = ipnQueueService;
        this.deadLetterIpnRepository = deadLetterIpnRepository;
    }

    //test endpoint
    @GetMapping("/add/{id}")
    public String add(@PathVariable Long id){
        IpnDTO ipnDTO = IpnDTO.builder()
                //.id(id)
                .ipnType(IpnType.PAY)
                .orderId("orderId")
                .paymentId(id)
                .transactionStatus(TransactionStatus.WAITING)
                .build();

        ipnQueueService.send(ipnDTO);

        return "success";
    }

    //TODO it could be change (http method)
    @PutMapping("/ipn/{id}")
    public void addDeadLetterIpnToQueue(@PathVariable  Long id){

        //DeadLetterIpn deadLetterIpn = deadLetterIpnRepository.findOne(id);
       DeadLetterIpn deadLetterIpn =  deadLetterIpnRepository.find(id);
       ipnQueueService.sendDeadLetterToSource(deadLetterIpn);
       deadLetterIpnRepository.delete(id);
    }

}
