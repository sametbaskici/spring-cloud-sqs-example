package com.example.sam.springcloud.sqs.converter;

import com.example.sam.springcloud.sqs.domain.DeadLetterIpn;
import com.example.sam.springcloud.sqs.dto.IpnDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DeadLetterIpnToIpnDTOConverter implements Converter<DeadLetterIpn, IpnDTO> {


    @Override
    public IpnDTO convert(DeadLetterIpn source) {

        if(Objects.isNull(source)){
            throw new RuntimeException("source is null");
        }

        return IpnDTO.builder()
                .id(source.getId())
                .ipnType(source.getIpnType())
                .orderId(source.getOrderId())
                .transactionStatus(source.getTransactionStatus())
                .paymentId(source.getPaymentId())
                .build();

    }


}
