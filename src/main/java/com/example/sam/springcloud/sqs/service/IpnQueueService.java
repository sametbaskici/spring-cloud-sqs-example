package com.example.sam.springcloud.sqs.service;

import com.example.sam.springcloud.sqs.converter.DeadLetterIpnToIpnDTOConverter;
import com.example.sam.springcloud.sqs.converter.IpnDTOToDeadLetterIpnConverter;
import com.example.sam.springcloud.sqs.domain.DeadLetterIpn;
import com.example.sam.springcloud.sqs.domain.IpnType;
import com.example.sam.springcloud.sqs.dto.IpnDTO;
import com.example.sam.springcloud.sqs.repository.DeadLetterIpnRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.core.SqsMessageHeaders;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class IpnQueueService implements QueueService<IpnDTO> {

    @Value("${cloud.aws.queue.name}")
    private String ipnQueueName;


    private DeadLetterIpnRepository deadLetterIpnRepository;
    private QueueMessagingTemplate queueMessagingTemplate;
    private DeadLetterIpnToIpnDTOConverter deadLetterIpnToIpnDTOConverter;
    private IpnDTOToDeadLetterIpnConverter ipnDTOToDeadLetterIpnConverter;

    public IpnQueueService(DeadLetterIpnRepository deadLetterIpnRepository, QueueMessagingTemplate queueMessagingTemplate, DeadLetterIpnToIpnDTOConverter deadLetterIpnToIpnDTOConverter, IpnDTOToDeadLetterIpnConverter ipnDTOToDeadLetterIpnConverter) {
        this.deadLetterIpnRepository = deadLetterIpnRepository;
        this.queueMessagingTemplate = queueMessagingTemplate;
        this.deadLetterIpnToIpnDTOConverter = deadLetterIpnToIpnDTOConverter;
        this.ipnDTOToDeadLetterIpnConverter = ipnDTOToDeadLetterIpnConverter;
    }

    @Override
    public void send(IpnDTO ipn) {

        Map<String, Object> headers = new HashMap<>();
        headers.put(SqsMessageHeaders.SQS_GROUP_ID_HEADER, "reisGroup");
        headers.put(SqsMessageHeaders.SQS_DEDUPLICATION_ID_HEADER, UUID.randomUUID().toString());

        queueMessagingTemplate.convertAndSend(ipnQueueName, ipn, headers);

    }

    @Override
    public List<IpnDTO> getDeadLetterIpns(IpnType ipnType) {

        return deadLetterIpnRepository.findByIpnType(ipnType).stream()
                .map(deadLetterIpnToIpnDTOConverter::convert)
                .collect(Collectors.toList());
    }

    public void sendDeadLetterToSource(DeadLetterIpn deadLetterIpn){
        IpnDTO ipnDTO = deadLetterIpnToIpnDTOConverter.convert(deadLetterIpn);
        send(ipnDTO);
        log.info("ipn resend to queue");
    }

    //TODO listen islemleri controller da olabilir

    @SqsListener("ipnDeadLetterQueue.fifo")
    private void deadLetterListener(IpnDTO ipn,  @Headers Map<String,Object> headers){
        DeadLetterIpn ipnDeadletter = ipnDTOToDeadLetterIpnConverter.convert(ipn);
        deadLetterIpnRepository.save(ipnDeadletter);
    }

    //@SqsListener(value = "IpnQueue.fifo", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    //private void listener(IpnDTO ipn,  @Headers Map<String,Object> headers){
//
    //    try{
    //        if(true) throw new RuntimeException("listener exception"); //TODO REMOVE
//
    //    }catch (Exception e){
    //        log.error(e.getMessage());
    //    }
//
    //    log.info(ipn.toString());
    //    //TODO Send ipn to merchant
//
    //}

}
