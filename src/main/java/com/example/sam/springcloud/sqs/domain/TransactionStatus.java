package com.example.sam.springcloud.sqs.domain;

public enum TransactionStatus {
    APP, WAITING, SUCCESS
}
