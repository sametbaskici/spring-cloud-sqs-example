package com.example.sam.springcloud.sqs.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@Builder
@Data
@Entity
@NoArgsConstructor
public class DeadLetterIpn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private IpnType ipnType;

    private Long paymentId;

    private String orderId;

    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;

}
