package com.example.sam.springcloud.sqs.converter;

import com.example.sam.springcloud.sqs.domain.DeadLetterIpn;
import com.example.sam.springcloud.sqs.dto.IpnDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class IpnDTOToDeadLetterIpnConverter implements Converter<IpnDTO, DeadLetterIpn> {


    @Override
    public DeadLetterIpn convert(IpnDTO source) {

        if(Objects.isNull(source)){
            throw new RuntimeException("source is null");
        }

        return DeadLetterIpn.builder()
                .id(source.getId())
                .ipnType(source.getIpnType())
                .orderId(source.getOrderId())
                .paymentId(source.getPaymentId())
                .build();
    }
}
